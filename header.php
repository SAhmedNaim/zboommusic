<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <!-- Basic Page Needs
  ================================================== -->
	<meta charset="<?php bloginfo('chrset'); ?>">
	<meta name="description" content="<?php bloginfo('charset'); ?>">
	<meta name="author" content="www.sahmednaim.com">
	
    <!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- CSS
  ================================================== -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>">
	
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
		<script src="js/html5.js"></script>
		<script src="js/css3-mediaqueries.js"></script>
	<![endif]-->
	
	<link href='./images/favicon.ico' rel='icon' type='image/x-icon'/>

	<style type="text/css">
		<?php 
			global $zBoom;
			echo $zBoom['opt-style'];
		?>

		body, p {
			color: <?php echo $zBoom['opt-color']; ?> !important;
		}

		article a {
			color: <?php echo $zBoom['opt-link-color']; ?> !important;
		}

		body {
			/* Background */
			background-color: <?php echo $zBoom['opt-background']['background-color'] ?> !important;
			background-repeat: <?php echo $zBoom['opt-background']['background-repeat'] ?> !important;
			background-size: <?php echo $zBoom['opt-background']['background-size'] ?> !important;
			background-attachment: <?php echo $zBoom['opt-background']['background-attachment'] ?> !important;
			background-position: <?php echo $zBoom['opt-background']['background-position'] ?> !important;
			background-image: url(<?php echo $zBoom['opt-background']['background-image'] ?>) !important;
		}

		nav .wrap-nav {
			border-width: <?php echo $zBoom['opt-nav-border']['border-top']; ?>;
			border-style: <?php echo $zBoom['opt-nav-border']['border-style']; ?>;
			border-color: <?php echo $zBoom['opt-nav-border']['border-color']; ?>;
		}

		header .wrap-header {
			width: <?php echo $zBoom['opt-nav-dimensions']['width'] ?> !important;
			height: <?php echo $zBoom['opt-nav-dimensions']['height'] ?> !important;
		}
	</style>

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<!-- ------------Header------------- -->
<header>
	<div class="wrap-header zerogrid">
		<div id="logo">

			<?php if($zBoom['opt-logo-visibility'] == 1) : ?>

				<a href="<?php bloginfo('home'); ?>">
					<img src="
						<?php
							global $zBoom;
							echo $zBoom['opt-logo']['url']; 
						?>
					" width="200px" height="100px"/>
				</a>

			<?php endif; ?>

		</div>

		<!-- <?php get_search_form(); ?> -->
		
		<div id="search">
			<div class="button-search"></div>

			<form method="get" action="<?php esc_url(bloginfo('home')); ?>">
				<input type="text" name="s" value="Search..." onfocus="if (this.value == &#39;Search...&#39;) {this.value = &#39;&#39;;}" onblur="if (this.value == &#39;&#39;) {this.value = &#39;Search...&#39;;}">
			</form>

		</div>
	</div>
</header>

<nav>
	<div class="wrap-nav zerogrid">
		<div class="menu">

			<?php 
				if(function_exists('wp_nav_menu'))
				{
					wp_nav_menu( array(
						'theme_location'  => 'main-menu'
					) );
				}
			?>

		</div>
		
		<!-- <div class="minimenu"><div>MENU</div>
			<select onchange="location=this.value">
				<option></option>
				<option value="index.html">Home</option>
				<option value="blog.html">Blog</option>
				<option value="gallery.html">Gallery</option>
				<option value="single.html">About</option>
				<option value="contact.html">Contact</option>
			</select>
		</div> -->
	</div>
</nav>
