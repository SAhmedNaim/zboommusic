Source:

#Icon -> 

 - Dashicons: https://developer.wordpress.org/resource/dashicons/#welcome-widgets-menus

 - IconFinder: https://www.iconfinder.com/

 - Elusive Icons: http://elusiveicons.com/icons/

 - Font Awesome: https://fontawesome.com/v4.7.0/

#Redux Framework -> 

 - Documentation: https://docs.reduxframework.com/

 - Demo: https://demo.redux.io/wp-login.php

 - 