<?php 
/*
Template Name: Test
*/

get_header(); ?>


<!-- ------------Content------------- -->
<section id="content">
	<div class="wrap-content zerogrid">
		<div class="row block03">
			
			<?php global $zBoom; if($zBoom['opt-layout'] == 1) : ?>
				
				<div class="col-3-3">
					<div class="wrap-col">
						<?php while(have_posts()) : the_post(); ?>

						<article>
							<?php the_post_thumbnail( 'thumbnail' ); ?>
							<h2>
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>		
								</a>
							</h2>

							<div class="info">
								[By <?php the_author(); ?> 
								on <?php the_time('F d Y'); ?> 
								with <?php comments_popup_link(); ?>]
							</div>
							<p><?php the_content(); ?></p>
							<div class="comment">
								<?php comments_template(); ?>
							</div>
						</article>

						<?php endwhile; ?>
					</div>
				</div>
			
			<?php endif; ?>

			<?php if($zBoom['opt-layout'] == 2) : ?>

				<div class="col-1-3">
					<div class="wrap-col">
						<div class="box">
							<div class="heading"><h2>Latest Albums</h2></div>
							<div class="content">
								<img src="http://localhost/wordpress/wp-content/themes/zBoomMusic/images/albums.png" width="100%"/>
							</div>
						</div>
						<div class="box">
							<div class="heading"><h2>Upcoming Events</h2></div>
							<div class="content">
								<div class="list">
									<ul>
										<li><a href="#">Magic Island Ibiza</a></li>
										<li><a href="#">Bamboo Is Just For You</a></li>
										<li><a href="#">Every Hot Summer</a></li>
										<li><a href="#">Magic Island Ibiza</a></li>
										<li><a href="#">Bamboo Is Just For You</a></li>
										<li><a href="#">Every Hot Summer</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-2-3">
					<div class="wrap-col">

						<?php while(have_posts()) : the_post(); ?>

						<article>
							<?php the_post_thumbnail( 'thumbnail' ); ?>
							<h2>
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?>		
								</a>
							</h2>
							
							My Favourite Sports: 
							<?php 
								// As output is same, so any of following line can be used. 
								echo get_post_meta( get_the_ID(), 'favourite-game', true ); 
								// echo get_post_meta( $post->ID, 'favourite-game', true ); 
							?>

							<div class="info">
								[By <?php the_author(); ?> 
								on <?php the_time('F d Y'); ?> 
								with <?php comments_popup_link(); ?>]
							</div>
							<p><?php the_content(); ?></p>
							<div class="comment">
								<?php comments_template(); ?>
							</div>
						</article>

						<?php endwhile; ?>

					</div>
				</div>

			<?php endif; ?>

			<ul>
				
				<?php if($zBoom['opt-social']['1']) : ?>

					<li>
						<a href="<?php echo $zBoom['opt-social']['1'] ?>" target="_blank">
							<i class="fa fa-facebook" ></i>
						</a>
					</li>

				<?php endif; ?>

				<?php if($zBoom['opt-social']['2']) : ?>

					<li>
						<a href="<?php echo $zBoom['opt-social']['2'] ?>" target="_blank">
							<i class="fa fa-twitter" ></i>
						</a>
					</li>

				<?php endif; ?>

				<?php if($zBoom['opt-social']['3']) : ?>

					<li>
						<a href="<?php echo $zBoom['opt-social']['3'] ?>" target="_blank">
							<i class="fa fa-google-plus" ></i>
						</a>
					</li>

				<?php endif; ?>

				<?php if($zBoom['opt-social']['4']) : ?>

					<li>
						<a href="<?php echo $zBoom['opt-social']['4'] ?>" target="_blank">
							<i class="fa fa-linkedin" ></i>
						</a>
					</li>

				<?php endif; ?>
				
			</ul>

			Gender: 
			<?php if($zBoom['opt-gender'] == '1') : ?>
				Male
			<?php else : ?>
				Female
			<?php endif; ?>

			<hr/>

			Qualification:
			<?php if($zBoom['opt-qualification'] == '1') : ?>
				S.S.C
			<?php endif; ?>
			<?php if($zBoom['opt-qualification'] == '2') : ?>
				H.S.C
			<?php endif; ?>
			<?php if($zBoom['opt-qualification'] == '3') : ?>
				B.Sc
			<?php endif; ?>
			<?php if($zBoom['opt-qualification'] == '4') : ?>
				M.Sc
			<?php endif; ?>
			<?php if($zBoom['opt-qualification'] == '5') : ?>
				Ph.D
			<?php endif; ?>

			<hr/>

			Favourite Category: 

			<?php 
				if($zBoom['opt-favourite-category'][0]) : 
					echo get_the_category_by_id( $zBoom['opt-favourite-category'][0] );
				endif; 
			?>
			
			<?php 
				if($zBoom['opt-favourite-category'][1]) : 
					echo get_the_category_by_id( $zBoom['opt-favourite-category'][1] );
				endif; 
			?>

			<hr/>
			
			You are donating:
			<?php echo $zBoom['opt-donate']; ?>

			<hr/>

			<?php dynamic_sidebar( 'testpage-widget' ); ?>

			<hr/>

			<h2>
				My Favourite Game: 
				<?php echo get_post_meta( get_queried_object_id(), 'favourite-game', true ); ?>
			</h2>

			<h2>
				My Favourite Food: 
				<?php  echo get_post_meta( get_queried_object_id(), 'fav-food', true ); ?>
			</h2>

			<h2>
				My Favourite Color: 
				<?php  echo get_post_meta( get_queried_object_id(), 'fav-color', true ); ?>
			</h2>

			<hr/>
			
			<h1>Education: </h1>
			<h2>
				My School: 
				<?php  echo get_post_meta( get_queried_object_id(), 'education_school', true ); ?>
			</h2>

			<h2>
				My College: 
				<?php  echo get_post_meta( get_queried_object_id(), 'education_college', true ); ?>
			</h2>

			<h2>
				My University: 
				<?php  echo get_post_meta( get_queried_object_id(), 'education_university', true ); ?>
			</h2>
			
		</div>
	</div>
</section>


<?php get_footer(); ?>