﻿<?php get_header(); ?>


<div class="featured">
	<div class="wrap-featured zerogrid">
		<div class="slider">
			<div class="rslides_container">
				<ul class="rslides" id="slider">
					<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/slide1.png"/></li>
					<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/slide2.png"/></li>
					<li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/slide3.png"/></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<!-- ------------Content------------- -->
<section id="content">
	<div class="wrap-content zerogrid">
		<div class="row block01">
			<div class="col-1-3">
				<div class="wrap-col box">
					<h2>The White Night</h2>
					<p>Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis.</p>
					<div class="more"><a href="#">[...]</a></div>
				</div>
			</div>
			<div class="col-1-3">
				<div class="wrap-col box">
					<h2>Tons of Fans</h2>
					<p>Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis.</p>
					<div class="more"><a href="#">[...]</a></div>
				</div>
			</div>
			<div class="col-1-3">
				<div class="wrap-col box">
					<h2>Best DJ's Ever</h2>
					<p>Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis.</p>
					<div class="more"><a href="#">[...]</a></div>
				</div>
			</div>
		</div>
		<div class="row block02">
			<div class="col-2-3">
				<div class="wrap-col">
					<div class="heading"><h2>Latest Blog</h2></div>

					<?php while(have_posts()) : the_post(); ?>

					<article class="row">
						<div class="col-1-3">
							<div class="wrap-col">
								<?php echo the_post_thumbnail( 'thumbnail' ); ?>
							</div>
						</div>
						<div class="col-2-3">
							<div class="wrap-col">
								<h2><a href="<?php the_permalink(); ?>">
									<?php the_title(); ?></a>
								</h2>
								<div class="info">
									By <?php the_author(); ?> 
									on <?php the_time('M d, Y || g:i a'); ?> 
									with <?php comments_popup_link(); ?>	
								</div>
								<p>
									<?php read_more(30); ?>
									[...] <a href="<?php the_permalink(); ?>">Read More
								</p>
							</div>
						</div>
					</article>
					
					<?php endwhile; ?>

					<div id="pagi">
					<?php 
						the_posts_pagination( array(
							'show_all'				=> false,
							'prev_text' 			=> 'PREV',
							'next_text'				=> 'NEXT',
							'screen_reader_text'	=> ' ',
							'before_page_number'	=> '<b>',
							'after_page_number'		=> '</b>'
						) ); 
					?>
					</div>
					
				</div>
			</div>
			<div class="col-1-3">

				<?php get_sidebar(); ?>
			
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>