﻿<?php get_header(); ?>

<?php 
/*
Template Name: Home
*/
?>


<div class="featured">
	<div class="wrap-featured zerogrid">
		<div class="slider">
			<div class="rslides_container">
				<ul class="rslides" id="slider">

					<?php 
						$sliderItems = new WP_Query( array(
							'post_type'			=> 'zBoomSlider',
							'posts_per_page'	=> -1
						) );
					?>

					<?php while($sliderItems->have_posts()) : $sliderItems->the_post(); ?>
						<li><?php the_post_thumbnail(); ?></li>
					<?php endwhile; ?>

				</ul>
			</div>
		</div>
	</div>
</div>

<!-- ------------Content------------- -->
<section id="content">
	<div class="wrap-content zerogrid">
		<div class="row block01">

			<?php 
				$serviceItem = new WP_Query( array(
					'post_type'			=> 'zBoomService',
					'posts_per_page'	=> 3
				) );

			?>

			<?php while($serviceItem->have_posts()) : $serviceItem->the_post(); ?>

			<div class="col-1-3">
				<div class="wrap-col box">
					<h2><?php the_title(); ?></h2>
					<p><?php read_more(10); ?></p>
					<div class="more"><a href="<?php the_permalink(); ?>">[...]</a></div>
				</div>
			</div>
			
			<?php endwhile; ?>



		</div>
		<div class="row block02">
			<div class="col-2-3">
				<div class="wrap-col">
					<div class="heading"><h2>Latest Blog</h2></div>

					<?php 
						$post_contents = new WP_Query( array(
							'post_type'			=> 'post',
							'posts_per_page'	=> 5
						) );
					?>

					<?php while($post_contents->have_posts()) : $post_contents->the_post(); ?>

					<article class="row">
						<div class="col-1-3">
							<div class="wrap-col">
								<?php echo the_post_thumbnail( 'thumbnail' ); ?>
							</div>
						</div>
						<div class="col-2-3">
							<div class="wrap-col">
								<h2><a href="<?php the_permalink(); ?>">
									<?php the_title(); ?></a>
								</h2>
								<div class="info">
									By <?php the_author(); ?> 
									on <?php the_time('M d, Y || g:i a'); ?> 
									with <?php comments_popup_link(); ?>	
								</div>
								<p>
									<?php read_more(30); ?>
									[...] <a href="<?php the_permalink(); ?>">Read More
								</p>
							</div>
						</div>
					</article>
					
					<?php endwhile; ?>


					<?php global $zBoom; ?>
					<ul>

						<?php if($zBoom['opt-programming-language']['1'] == 1) : ?>
							<li>Java</li>
						<?php endif ?>

						<?php if($zBoom['opt-programming-language']['2'] == 1) : ?>
							<li>PHP</li>
						<?php endif ?>

						<?php if($zBoom['opt-programming-language']['3'] == 1) : ?>
							<li>C</li>
						<?php endif ?>

						<?php if($zBoom['opt-programming-language']['4'] == 1) : ?>
							<li>C++</li>
						<?php endif ?>

						<?php if($zBoom['opt-programming-language']['5'] == 1) : ?>
							<li>C#</li>
						<?php endif ?>

					</ul>
				</div>
			</div>
			<div class="col-1-3">
				<div class="wrap-col">
					
					<?php get_sidebar(); ?>
					
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>