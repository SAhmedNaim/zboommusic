<?php

// see test page
function zBoom_heading($atts, $content)
{
	$heading = shortcode_atts( array(
		'position'	=> 'left'
	), $atts );

	return '<h1 align="' . $heading['position'] . '">' . $content . '</h1>';
}
add_shortcode( 'heading', 'zBoom_heading' );


function zBoom_image($atts, $content)
{
	$image = shortcode_atts( array(
		'width'		=> '30px',
		'height'	=> '30px'
	), $atts );

	return '<img src="'. $content .'" width="' . $image['width'] . '" height="' . $image['height'] . '" alt="Image Missing" />';
}
add_shortcode( 'image', 'zBoom_image' );


// converting services section as a shortcode
function zBoom_services($atts, $content)
{
	ob_start(); 

	$service_content = extract(shortcode_atts( array(
		'number'	=> '3'
	), $atts ));
?>



<?php 
	$serviceItem = new WP_Query( array(
		'post_type'			=> 'zBoomService',
		'posts_per_page'	=> $number
	) );

?>

<?php while($serviceItem->have_posts()) : $serviceItem->the_post(); ?>

<div class="col-1-3">
	<div class="wrap-col box">
		<h2><?php the_title(); ?></h2>
		<p><?php read_more(10); ?></p>
		<div class="more"><a href="<?php the_permalink(); ?>">[...]</a></div>
	</div>
</div>

<?php endwhile; ?>


<?php
$services = ob_get_clean();
return $services;
}
add_shortcode( 'services', 'zBoom_services' );

