<?php 
/*
Template Name: Newspaper
*/

get_header();
?>

<section id="content">
	<div class="wrap-content zerogrid">
		<div class="row block03">
			<div class="one-half">
				
				<?php 

					$left_category_name = get_the_category_by_id( $zBoom['opt-left-category'] );

					// print_r($left_category_name);

					$left_category = new WP_Query( array(
						'post_type'			=> 'post',
						'posts_per_page'	=> 3,
						'category_name'		=> $left_category_name
					) );

					while($left_category -> have_posts()) : $left_category -> the_post(); 
				?>

						<p><?php the_title(); ?></p>

				<?php endwhile; ?>

			</div>
			<div class="one-half">
					
				<?php 
					$right_category_name = get_the_category_by_id( $zBoom['opt-right-category'] );

					// print_r($right_category_name);

					$right_category = new WP_Query( array(
						'post_type'			=> 'post',
						'posts_per_page'	=> 3,
						'category_name'		=> $right_category_name
					) );

					while($right_category -> have_posts()) : $right_category -> the_post(); 
				?>

						<p><?php the_title(); ?></p>
				
				<?php endwhile; ?>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>